 require 'spec_helper'
 require 'exam'
 #require_relative "../lib/exam.rb"
 #require_relative "../lib/exam/nodo.rb"
 #require_relative "../lib/exam/lista.rb"
 
 

describe Exam::SimpleQuestion do
	before :each do
		respuestas = {	"a" => 4,
						"b" => 5,
						"c" => 6,
						"d" =>"Ninguna de las anteriores"}
		@exam = Exam::SimpleQuestion.new(:question=> '2 + 5 = ?', :right=> respuestas['d'], :distraction=> respuestas)
		respuestas1 = {	"a" => "Una instancia de la clase Class",
						"b" => "Una constante",
						"c" => "Un objeto",
						"d" => "Ninguna de las anteriores"}
		@exam1 = Exam::SimpleQuestion.new(:question=> "Cuál es el tipo de objeto en el siguiente código ruby?:\n\tclass Objeto\n\tend", :right=> respuestas1['c'], :distraction=> respuestas1)
	end
	describe "# almacenamiento de los atributos" do
		it "Se almacena correctamente la pregunta" do
			#@exam.text.should eq("2 + 5 = ?")
			#expect(@exam.text,text) == '2 + 5 = ?'
			expect(@exam.question).to eq('2 + 5 = ?')
		end
		it "Se almacena correctamente la respuesta verdadera" do
			expect(@exam.right).to eq("Ninguna de las anteriores")
			#@exam.right.should eq(respuestas['d'])
		end
		it "Se almacenan bien todas las posibles respuestas" do
			@exam.distraction.should eq("a" => 4,
                                                "b" => 5,
                                                "c" => 6,
                                                "d" =>"Ninguna de las anteriores")
		end
		it "Se almacena bien la segunda pregunta" do
			@exam1.question.should eq("Cuál es el tipo de objeto en el siguiente código ruby?:\n\tclass Objeto\n\tend")
			@exam1.right.should eq("Un objeto")
			@exam1.distraction.should eq("a" => "Una instancia de la clase Class",
						"b" => "Una constante",
						"c" => "Un objeto",
						"d" => "Ninguna de las anteriores")
		end
	end
end


describe Exam::Nodo do
	before :each do
		@node = Exam::Nodo.new
		@node.value = 5 
	end
	it "El nodo almacena el valor y su siguiente" do
		@node.value.should eq(5)
		@node.next.should eq(nil)
	end
end

describe Exam::Llist do
	before :each do
		@list = Exam::Llist.new
	end
	
	it "Debe tener una cabeza y una cola" do
		@list.add(1)
		@list.add(2)
		@list.head.value.should eq(1)
		@list.tail.value.should eq(2)
		
	end
	it "Se debe insertar un elemento" do
		@list.add(20)
	end
	it "Se puede extraer el primer elemento" do
		@list.add(1)
		@list.add(2)
		@list.pop_head.should eq(1)
	end
	it "Se puede extraer el ultimo elemento" do
		@list.add(1)
		@list.add(2)
		@list.pop_tail.should eq(2)
	end
end

describe Exam::BoolQuestion do
	before :each do
		@p1 = Exam::BoolQuestion.new(:question=>"Es apropiado que una clase Tablero herede de una clase Juego?", :right=> false)
	end
	
	it "Comprobar herencia" do
		expect(@p1.instance_of?Exam::BoolQuestion).to eq(true)
	end
	
	it "Comprobar herencia" do
		expect(@p1.is_a?Exam::BoolQuestion).to eq(true)
	end
	
	it "Debe existir una pregunta" do
		@p1.question.should_not eq(nil)
	end
	it "Se ha de mostrar" do
		@p1.to_s.should eq("Es apropiado que una clase Tablero herede de una clase Juego?\n -Verdadero\n -Falso")
	end
	it "Se puede contestar de forma esperada" do
		@p1.answer(false).should eq(true)
	end
end