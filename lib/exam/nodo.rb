module Exam
    
    class Nodo < Struct.new(:value, :next, :prev) #Class Nodo simula un elemento nodo con valor y siguiente nodo
   		
   		def initialize
			@value = nil
			@next = nil
			@prev = nil
		end
	
		def to_s
			"#{@value} <--> "
		end 
	
    end
    
end
