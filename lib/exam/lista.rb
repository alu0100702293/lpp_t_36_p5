module Exam
    
   class Llist #Class Llist implementa una lista enlazada de Nodo
		attr_accessor :head, :tail
		def initialize
	  		@head = nil
	  		@tail = nil
		end
	
		def add (value) #Metodo que inserta un valor al principio de la lista
	  		if @head == nil
				@head = Nodo.new
				@head.value = value
				@tail = nil
			elsif @head.next == nil
				@tail = Nodo.new
				@tail.value = value
				@head.next = @tail
				@tail.prev = @head
			elsif 
				tmp = @tail
				@tail = Nodo.new
				@tail.value = value
				@tail.prev = tmp
				tmp.next = @tail
			end
		end
		
		def pop_head #Extrae el primer elemento de la lista
			return false if @head == nil
			pop = @head.value
			@head = @head.next
			@head.prev = nil
			pop
		end
		
		def pop_tail
		    return false if @head == nil
		    pop = @tail.value
		    tmp = @tail
		    @tail = tmp.prev
		    @tail.next = nil
		    pop
		end
		
		def buscar (value) #Busca un valor en la lista
			actual = @head
			while actual != nil
				return actual if actual.value == value
				actual = actual.next
			end

			nil
		end
		
		def mostrar #devuelve nodos en una lista
			actual = @head
			str = []
			while actual
				str << actual.to_s
				actual = actual.next
			end

			str
		end

		
    end
    

    
end