require "exam/version"

require "exam/lista.rb"
require "exam/nodo.rb"

module Exam
	class SimpleQuestion #Class Exam contiene informacion sobre las preguntas de un examen y sus soluciones
	    attr_accessor :question, :right, :distraction
	#attr_accessor :distraction
		def initialize(args)
		    @question = args[:question]
		    #raise ArgumentError, 'Specify :text, :right and :distractor'
		    @right = args[:right]
		    @distraction = args[:distraction]
		end
	    
		def to_s #Devuelve la pregunta y las posibles respuestas como string para mostrarse por pantalla
		    "#{question}\n#{distraction}"
		end
    end
	
	class BoolQuestion < SimpleQuestion #Class BoolQuestion contiene una pregunta y la solucion verdadero o falso
	
		def initialize(args)
		    @question = args[:question]
		    #raise ArgumentError, 'Specify :text, :right and :distractor'
		    @right = args[:right]
		    @ditraction = { "a" => true, "b" => false }
		end
	    
		def answer(reply) #Responde a la pregunta, devuelve true o false dependiendo de si la respuesta es correcta o no
		    if reply == @right
			true
		    elsif
			false
		    end
		end
		
		def to_s
			"#{@question}\n -Verdadero\n -Falso"
		end
		
	end

end
